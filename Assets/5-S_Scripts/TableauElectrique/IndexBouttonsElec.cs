﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexBouttonsElec : MonoBehaviour
{
    public int indexPowerButton;

    public bool buttonActive;

    private TableauElec tableauElec;
    private Fusible fusible;

    public Color colorButtonDesactive;
    public Color colorButtonActive;
    public Color colorButtonPush;

    private Vector3 iniVectorPart;

    public void Start()
    {
        tableauElec = GameObject.FindObjectOfType<TableauElec>();
        fusible = GameObject.FindObjectOfType<Fusible>();

        iniVectorPart = GetComponentInChildren<Transform>().transform.position;
    }

    public void OnMouseDown()
    {
        tableauElec.curPower += indexPowerButton;
    }

    public void PressFonction()
    {
        if (!buttonActive)
        {
            tableauElec.curPower += indexPowerButton;
        }
    }

    public void Update()
    {
        if (!fusible.fusibleOK)
        {
            GetComponentInChildren<Renderer>().material.EnableKeyword("_EmissionColor");
            GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", colorButtonDesactive);
        }
        else
        {
            GetComponentInChildren<Renderer>().material.DisableKeyword("_EmissionColor");
            GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", colorButtonActive);
        }

        if (GetComponent<Valve.VR.InteractionSystem.HoverButton>().engaged == true)
        {
            if (!buttonActive)
            {
                tableauElec.curPower += indexPowerButton;
            }

            buttonActive = true;
        }

        if (buttonActive)
        {
            GetComponentInChildren<Transform>().transform.position = iniVectorPart;

            GetComponentInChildren<Renderer>().material.EnableKeyword("_EmissionColor");
            GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", colorButtonPush);
        }
    }
}
